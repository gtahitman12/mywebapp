import { Injectable } from '@angular/core';
import { Mockbook } from '../component/book/mockbook';
import { Flight } from '../component/book/book';
@Injectable({
  providedIn: 'root'
})
export class PageServiceService {
  flight:Flight[]=[];
  newflight:Flight[]=[]
  constructor() {
    this.flight=Mockbook.mbook;
  }
  getpage():Flight[]{
    return this.flight;
  }
  setpage(fullName:string,from:string,to:string,type:string,departure:Date,arrival:Date,adults:number,children:number,infants:number):void{
    this.flight.push({fullName,from,to,type,departure,arrival,adults,children,infants})
  }
}
