import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { PageServiceService } from 'src/app/share/page-service.service';
import { Flight } from '../book/book';


@Component({
  selector: 'app-bookpage',
  templateUrl: './bookpage.component.html',
  styleUrls: ['./bookpage.component.css']
})
export class BookpageComponent implements OnInit {
  order !:Flight[];
  orderForm !:FormGroup;
  today: Date;
  formfrom !: string;
  formto !: string;
  mindate: Date;
  isshow=false;
  constructor(private fb:FormBuilder,private pageService:PageServiceService) {
    this.orderForm=this.fb.group({
      name:['',Validators.required],
      from:['',Validators.required],
      to:['',Validators.required],
      type:['',Validators.required],
      adult:['',Validators.required,Validators.max],
      departure:['',Validators.required],
      children:['',Validators.required,Validators.max],
      infants:['',Validators.required],
      arrival:['',Validators.required]
    });
    this.getpage();
    this.setmindate();
    this.today=new Date();
    this.mindate=new Date();
    this.formfrom="null";
    this.formto="null";

  }
  getpage(){
    this.order=this.pageService.getpage();
  }
  sendorder(){
    this.pageService.setpage(this.orderForm.value.name,this.orderForm.value.from,this.orderForm.value.to ,this.orderForm.value.type,this.orderForm.value.departure,this.orderForm.value.arrival,this.orderForm.value.adult,this.orderForm.value.children,this.orderForm.value.infants);
  }
  setmindate(){
    this.mindate=this.orderForm.value.departure;
  }
  setfrom(){
    this.formfrom=this.orderForm.value.from;
  }
  setto(){
    this.formto=this.orderForm.value.to;
  }
  ngOnInit(): void {
  }

}
