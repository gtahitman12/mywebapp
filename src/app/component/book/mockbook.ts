import { Flight } from "./book";

export class Mockbook {
  public static mbook : Flight[]=[
      {
        fullName:"choong",
        from: "BANGKOK",
        to: "LONDON",
        type: "return",
        departure: new Date(2022,2,14),
        arrival: new Date(2022,2,30),
        adults: 1,
        children: 1,
        infants: 1,
      },
      {
        fullName:"john",
        from: "BANGKOK",
        to: "LONDON",
        type: "one-way",
        departure: new Date(2022,2,30),
        arrival: new Date(2022,2,31),
        adults: 1,
        children: 1,
        infants: 1,
      }
    ];

}
